/*****************************************************************************/
/* web_client_periodic.c                                                     */
/*                                                                           */
/* This program shows what the HTTP server sends to the client.  First,      */
/* it opens a TCP socket to the server.  Then, it sends the request          */
/* "GET <resource> HTTP/1.0\n\n" (the second newline is needed for the       */
/* "message-end" message.  Lastly it prints out the reply.                   */
/*                                                                           */
/* Example: ./web_client 127.0.0.1 1234                                      */
/*                                                                           */
/*****************************************************************************/

#include <stdarg.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <resolv.h>
#define _POSIX_C_SOURCE 199309L
#include <time.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include "utils.h"

#define MAXBUF  1024
#define PANIC(msg)  {perror(msg); abort();}

void mysleep_ms(int milisec) {
    printf("sleep!\n");
    struct timespec res;
    res.tv_nsec = (milisec % 1000) * 1000000; 
    res.tv_sec = milisec / 1000;
    nanosleep( &res, NULL); 
}

int main(int argc, char *argvs[])
{
  int sockfd, bytes_read;
  struct sockaddr_in dest;
  char buffer[MAXBUF];
  struct timespec start_latency, finish_latency, start_handling, finish_handling;
  double elapsed;
  double latency[20];


  clock_gettime(CLOCK_MONOTONIC, &start_handling); //Start sending


  for(int i = 0; i < 20; i++) {
    if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0 )
        return -1;
    

    /*---Initialize server address/port struct---*/
    bzero(&dest, sizeof(dest));
    dest.sin_family = AF_INET;
    dest.sin_port = htons(1234); /*default HTTP Server port */
    dest.sin_addr.s_addr = inet_addr(argvs[1]);

    /*---Start timer---*/
    clock_gettime(CLOCK_MONOTONIC, &start_latency);

    /*---Connect to server---*/
    CHECK_NZ ( connect(sockfd, (struct sockaddr*)&dest, sizeof(dest)) )

    sprintf(buffer, "GET %s HTTP/1.0\n\n", argvs[2]);
    send(sockfd, buffer, strlen(buffer), 0);

    /*---While there's data, read and print it---*/
    do
        {
        bzero(buffer, sizeof(buffer));
        bytes_read = recv(sockfd, buffer, sizeof(buffer), 0);
        
        if ( bytes_read > 0 ) {
            clock_gettime(CLOCK_MONOTONIC, &finish_latency); // Stop when recieved answer
            printf("%s", buffer);
        }
        }
    while ( bytes_read > 0 );

    /*---Clean up---*/
    close(sockfd);    
    
    /*---Save latency---*/
    elapsed = (finish_latency.tv_sec - start_latency.tv_sec);
    elapsed += (finish_latency.tv_nsec - start_latency.tv_nsec) / 1000000000.0;
    latency[i] = elapsed;



    printf("%f \n",latency[i]);

    /*---Sleep---*/
    mysleep_ms(300);

  }

  clock_gettime(CLOCK_MONOTONIC, &finish_handling); // To calculate request per second

  elapsed = (finish_handling.tv_sec - start_handling.tv_sec);
  elapsed += (finish_handling.tv_nsec - start_handling.tv_nsec) / 1000000000.0;
  double request_per_second = 20.0 / elapsed;

  
  printf("%f request handled per second", request_per_second);
  return 0;
}